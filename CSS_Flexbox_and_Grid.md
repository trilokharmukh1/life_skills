# Css FlexBox
Flexbox is a layout model which provides an easy and clean way to arrange items/div within a container.

If you want for instance five boxes in a row using the old block model then you have to set your margins and your padding and you have to do all types of math to figure out how much margin how much width you should have and it can get kind of it can be kind of a pain.

flexbox has taken care of all of this and it's responsive and mobile-friendly it looks good on smaller screens.

## Components of the flexbox
Flexbox has 2 components: 
* Flex Container : 
    * This is a parent div that contains items.
    * To create a flex container we need to set **display: flex** or **inline-flex** value.
* Flex Items : 
    * This is direct children of the flex container.

 ![container_item](./images/container_Item.png)
 

## Axes of flexbox
There are two Axes of flexbox
* Main axis
* Cross axis
![Axes of Flesbox](./images/flexbox_Axis.png)

### Main Axis
* By default on the main axis items are placed left to right
* The main axis is defined by the flex-direction property
* It has four possible values:
    * row: to place element from left to right.
    * row-reverse: to place element from right to left.
    * column: to place elements from top to bottom.
    * column-reverse: to place element from bottom to top.

    ![main_axis](./images/main_axis.jpg)

### Cross Axis
* In the cross-axis items are placed perpendicular to the main axis.
* If your flex-direction (main axis) is set to row or row-reverse the cross-axis runs down the columns.
* If your main axis is column or column-reverse then the cross axis runs along the rows.
![cross_Axis](./images/cross_axis.jpg)

## flex-direction
* Using the flex-direction property we can specify the direction of flexible items.
* It also defines how flexbox items are ordered within a flexbox container.
* Some of the flex-direction properties
    * flex-direction : row; (left to right)
    * flex-direction : row-reverse; (right to left reverse order)
    * flex-direction : column; (up to down)
    * flex-direction : column-reverse; (down to up reverse order)

```HTML
<!DOCTYPE html>
<html>
<head>
    <title>Flexbox</title>
    <style>
    .flex-container {
        display: flex;       
        flex-direction: row;
        /* flex-direction: row-reverse; */
        /* flex-direction: column; */
        /* flex-direction: column-reverse; */
        background-color: #32a852;
    }
    
    .flex-container div {
        background-color: #c9d1cb;
        margin: 10px;
        padding: 10px;
        width: 70px;
    }
    </style>
</head>

<body>
    <h2>Flexbox</h2>
    <h4>flex-direction: row;</h4>
    <div class="flex-container">
        <div>Item1</div>
        <div>Item2</div>
        <div>Item3</div>
    </div>
</body>

</html>

```
![flex-direction:row](./images/main_axis-row.png) ![flex-direction:row-reverse](./images/main_axis-row_reverse.png)
![flex-direction:column](./images/main-axis_col.png)
![flex-direction:column-reverse](./images/main-axis_col_reverse.png)

## flex-wrap
The initial flexbox concept is the container to set its items in one single line.
### Values
* flex-wrap: nowrap;
    * Flex items are displayed in one row.
* flex-wrap: wrap;
    * Flex items are displayed in multiple rows if needed from left to right and top-to-bottom.
* flex-wrap: wrap-reverse;
    * Flex items are displayed in multiple rows if needed from left to right and bottom-to-top.

## flexbox properties

* justify-content:flex-start | flex-end | center | space-between | space-around;

* align-items:stretch | flex-start | flex-end |center | baseline;

* align-content:stretch | flex-start | flex-end |center | space-between | space-around;

* order: (number); default is 0

* flex-grow:(number); default is 0 (it will not be allowed to grow)

* align-self: auto | flex-start | flex-end | center |
baseline | stretch;


# CSS grid
Like flexbox, CSS Grid is a layout system that lets us add items both horizontally and vertically on a webpage.

That's different from Flexbox, where we could only place items in one dimension at a time.

CSS Grid is a powerful tool that allows for two-dimensional layouts to be created on the web.

## Grid Container
* To use CSS Grid, we first have to create a parent element that acts as a container.

 * We create a grid container by declaring display: grid or display: inline-grid on an element.

 ## Grid tracks
 * A grid track is the space between any two lines on the grid.
 * We define rows on our grid with the grid-template-rows properties.
 * We define columns on our grid with the grid-template-columns properties. 
 
 ### grid-template-row:
 * The grid-template-rows property specifies the number of rows in a grid layout.
 * Track size values can be any non-negative, length value (px, %, em, etc.)
 * If we have not defined track height they are defined by the contents of each.
 ``` HTML
 <!DOCTYPE html>
<html>
<head>
    <title>CSS grid</title>
    <style>
    .grid-container {
        display: grid;       
     grid-template-rows: 50px 100px;
        background-color: #e9e8aa;
        width: 300px;
    }
    
    .grid-item{
        background-color: #eb644d;
        margin: 10px;
        padding: 10px;
     width: 100px;
    }
    </style>
</head>

<body>
    <h2>CSS grid</h2>
    <h4>grid-template-rows</h4>
    <div class="grid-container">
        <div class="grid-item">Item 1</div>
        <div class="grid-item">Item 2</div>
        <div class="grid-item">Item 3</div>
        <div class="grid-item">Item 4</div>
    </div>
</body>
</html>

 ```
 ![grid-template-rows](./images/grid-row.png)

### grid-template-columns:
* The grid-template-columns property specifies the number of columns in a grid layout.
* Like rows, a column track is created for each value specified for grid-template-columns.
* Items 3 and 4 were placed on a new row track because only 2 column track sizes were defined.
* And because they were placed in column tracks 1 and 2, their column sizes are equal to items 1 and 2.
``` CSS
.grid-container {
    display: grid;       
    grid-template-rows: 50px 100px;
    grid-template-columns: 90px 50px;
    background-color: #e9e8aa;
    width: 300px;
}
```
![grid-template-columns](./images/grid-columns.png)

### The fr unit
* The fr unit helps create flexible grid tracks. 
* fr unit represents a fraction of the available space in the grid container.
``` CSS
.grid-container {
    display: grid;       
    grid-template-columns: 1fr 1fr 2fr;
    background-color: #e9e8aa;
    width: 400px;
}
```
![grid_Using_fr-unit](./images/grid-fr%20unit.png)

### Repeating Grid Tracks
* This is useful for grids with items to repeat equal sizes or many items.
* Define repeating grid tracks using the repeat() notation. 
* The repeat() notation accepts 2 arguments: the first represents the number of times the defined tracks should repeat, and the second is the track definition

``` CSS
    .grid-container {
        display: grid;       
        grid-template-rows: repeat(2, 100px);
        grid-template-columns: repeat(2, 1fr 2fr);
        background-color: #e9e8aa;
        width: 700px;
    }
```
![Repeating-Grid-Tracks](./images/grid-repeat.png)

## Syntax of various grid properties:

 * grid-template-columns: none | auto | max-content | min-content | length | initial | inherit;

 * grid-template-rows: none | auto | max-content | min-content | length | initial | inherit;

 * grid-template-areas: none | item-name;

 * grid-template: none | grid-template-rows/grid-template-column | grid-template-areas | initial | inherit;

* grid-row-start: auto |row-line;

* grid-column-start: auto | span-n | column-line;

* grid-row-end: auto | span-n | row-line;

* grid-column-end: auto | span-n | column-line;

*  grid-area: row-start / column-start / row-end / column-end | item-name;

* grid-auto-columns: auto | max-content | min-content | length;

* grid-auto-rows: auto | max-content | min-content | length;

* grid-auto-flow: row | column | dense | row dense | column dense;

* grid-row: grid-row-start / grid-row-end;

* grid-column: grid-column-start / grid-column-end;

* row-gap: length | normal | initial | inherit;

* column-gap: length | normal | initial | inherit;

## References
* [Flexbox_Properties](https://css-tricks.com/snippets/css/a-guide-to-flexbox/ "")

* [Axes_of_flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)

* [Grid_tracks_1](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout)

* [Grid_tracks_2](https://learncssgrid.com/)

* [Syntax_of_grid_properties](https://linuxhint.com/css-grid-properties/)