# Learning Process

## What is the Feynman Technique? Paraphrase the video in your own words.

Feynman's learing Technique is if you understand somthing explain it on simple word. Feynman shared some key points that elaborated on the implementation of this concept.
* Take a piece of paper and write the name of the concept on the top.
* Try to explain the concept in simple language.
* Identify the problem areas and then go back to the sources to review.
* Review your explanation and try to eliminate any convoluted or technical terms and make it simple terms.

## What are the different ways to implement this technique in your learning process?
* Take a piece of paper and write the name of the concept on the top.
* Try to explain the concept in simple language.
* Identify the problem areas and then go back to the sources to review.
* Review your explanation and try to eliminate any convoluted or technical terms and make it simple terms.

## Paraphrase the video in detail in your own words.
From this video we can know how learning process can make efficient.

* Try to play on your strengths, that is if you take more time to understand something, don't think of this as your weakness but rather understand that you are learning whatever you are learning at a much deeper level.

* Think about a problem with your active focus but also take breaks in between so that your passive brain can think of new ways to solve the said issue.

## What are some of the steps that you can take to improve your learning process?

* While learning something new, take breaks in between, this lets the brain passively think on the topic and create neural pathways.

* Try to teach the topic that you are trying to learn in simple and non convoluted language.

* Identify the problem areas and then go back to the sources to review.

## Your key takeaways from the video? Paraphrase your understanding.

The video talks about how one should approach learning a new skill. The video emphasizes the point that one actually doesn't need hundreds of hours to learn a new skill and just needs 20 hours to learn enough that one can work with the skill.

## What are some of the steps that you can while approach a new topic?

Some of the steps that can be implemented while adopting a new skill are:-

* Deconstruct the skill, meaning learn about the different parts of the skill and prioritise the important topics first.

* Learn enough to self-correct, learn enough about something that you can correct yourself, and know the correct resources to learn.

* Remove practice barriers, that is do not judge yourself while learning a new skill if you get underconfident, it becomes harder to learn something.

* Learn something for at least for 20 hours.