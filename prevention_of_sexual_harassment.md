# Q1.) What kinds of behaviour cause sexual harassment?
 Ans.1.) Sexual harassment is any unwelcome verbal visual or physical conduct of a sexual nature that is severe or pervasive and affects the working conditions of the employees or creates a hostile work environment. 
Forms of Sexual harassment
* Verbal visual and physical verbal harassment.
* sexual innuendos threats spreading rumors about a person's personal or sexual life or using foul and obscene language
* visual harassment can include posters drawings pictures screen savers cartoons emails or texts of a sexual nature
* hysical harassment often includes sexual assault impeding or blocking movement inappropriate touching such as kissing hugging patting stroking or rubbing sexual gesturing or even leering or staring.
# Q2. ) What would you do in case you face or witness any incident or repeated incidents of such behaviour?
Ans.) 

* Record the dates on which you take any steps to try and address the problem, who you’ve spoken with – or met with – and their responses to you. Note all perceived acts of apparent retaliation, if any. (Be aware that you may later need to turn over a copy of this journal to others if a lawsuit is filed).
* Make a copy of any written reports or complaints that you decide to file. You must seriously consider asking a lawyer to review anything of this nature first.
* Confide in one or two long-term, trusted mentors about your situation – people who do not work where you do. 
* Ask your employer for an immediate copy of any reports being placed in your file about your current complaints.
* Each person needs to decide what action plan works best for him or herself, studies have found that informal actions result from the fastest solutions. One should directly confront the person involved to stop the unacceptable behavior, if this does not work, one should put this in writing. If one witnesses any sexual harassment, one should offer support to the victim and put a written complaint to the higher authorities.
